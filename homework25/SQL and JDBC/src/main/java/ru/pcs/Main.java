package ru.pcs;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

public class Main {

    public static void main(String[] args) {

        DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/pcs_2", "postgres", "5587");
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        ProductsRepository productsRepository = new ProductsRepositoryJbdcTemplateImpl(dataSource);
        System.out.println(productsRepository.findAll());
        System.out.println(productsRepository.findAllByPrice(30.50));
    }
}
