package ru.pcs;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

public class ProductsRepositoryJbdcTemplateImpl implements ProductsRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from goods order by id";

    //language=SQL
    private static final String SQL_SELECT_BY_PRICE = "select * from goods where cost = ";

    private JdbcTemplate jdbcTemplate;

    public ProductsRepositoryJbdcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    RowMapper<Product> productRowMapper = (row, rowNum) -> {
        int id = row.getInt("id");
        String description = row.getString("description");
        double cost = row.getDouble("cost");
        int goods_count = row.getInt("goods_count");
        return new Product(id, description, cost, goods_count);
    };

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        return jdbcTemplate.query(SQL_SELECT_BY_PRICE + price, productRowMapper);
    }
}
