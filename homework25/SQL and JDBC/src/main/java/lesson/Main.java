package lesson;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

public class Main {

    public static void main(String[] args) {

        DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/pcs_2","postgres", "5587");
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        UserRepository userRepository = new UserRepositoryJdbcTemplateImpl(dataSource);

        //User user = new User("Иван", "Иванов", 37);
        //userRepository.save(user);

        System.out.println(userRepository.findAll());

        User user2 = User.builder()
                .firstName("Vitaliy")
                .secondName("Kolosov")
                .age(31)
                .build();

        userRepository.save(user2);


    }

}
