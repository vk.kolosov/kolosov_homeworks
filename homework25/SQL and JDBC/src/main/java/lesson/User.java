package lesson;


import lombok.*;

//@Getter
//@Setter
//@ToString
//@EqualsAndHashCode -> it's all @Data
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User {

    private int id;
    private String firstName;
    private String secondName;
    private int age;

    public User(String firstName, String secondName, int age) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.age = age;
    }
}