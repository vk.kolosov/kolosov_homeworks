package lesson;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

public class UserRepositoryJdbcTemplateImpl implements UserRepository {

    //language=SQL
    private static final String SQL_INSERT = "insert into account (first_name, last_name, age) values (?, ?, ?)";
    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from account order by id";

    private JdbcTemplate jdbcTemplate;

    public UserRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static RowMapper<User> userRowMapper = (row, rowNum) -> {

      int id = row.getInt("id");
      String firstName = row.getString("first_name");
      String secondName = row.getString("last_name");
      int age = row.getInt("age");
      return new User(id, firstName, secondName, age);

    };

    @Override
    public List<User> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, userRowMapper);
    }

    @Override
    public void save(User user) {
        jdbcTemplate.update(SQL_INSERT, user.getFirstName(), user.getSecondName(), user.getAge());
    }

    @Override
    public List<User> findByAge(int userage) {
        return null;
    }

}
