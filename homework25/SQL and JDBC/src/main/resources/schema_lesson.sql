create table account (
    id serial primary key,

    first_name varchar(20),
    last_name varchar(20),
    age integer check (age > 0 and age < 120)
);
insert into account (first_name, last_name, age) values ('Freddy', 'Brown', 28);
insert into account (first_name, last_name, age) values ('Teddy', 'Brown', 26);
insert into account (first_name, last_name, age) values ('Tommy', 'Smith', 34);
insert into account (first_name, last_name, age) values ('Bob', 'Marley', 53);
insert into account (first_name, last_name, age) values ('Billy', 'Smith', 34);

update account set age = 27 where id = 1;

select * from account;

select first_name from account;

select first_name, age from account order by age;

select * from account where age > 30;

select count(*) from account where age > 30;

select distinct (age) from account;

select age, count(*) from account group by age;

create table car (
                     id serial primary key,
                     model varchar (20),
                     color varchar (20),
                     owner_id integer,
                     foreign key (owner_id) references account (id)
);

insert into car (model, color, owner_id) values ('Ford', 'Green', 2);
insert into car (model, color, owner_id) values ('Ferrary', 'Red', 4);
insert into car (model, color, owner_id) values ('Audi', 'Black', 5);
insert into car (model, color, owner_id) values ('Lada', 'Red', 5);
insert into car (model, color) values ('Bugatti', 'Blue');

select first_name, (select count(*) from car where owner_id = 5) as cars_count from account where id = 5;

select first_name, (select count(*) from car where owner_id = account.id) as cars_count from account;

select first_name
from account
where account.id
          in (select owner_id from car where color = 'Red');

select * from account a left join car c on a.id = c.owner_id;

select * from account a full join car c on a.id = c.owner_id;