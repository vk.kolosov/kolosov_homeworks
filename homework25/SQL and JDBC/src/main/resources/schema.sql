create table goods (
id serial primary key,
description varchar (20),
cost double precision,
goods_count integer
);

insert into goods (description, cost, goods_count) values ('Banana', 30.50 , 1400);
insert into goods (description, cost, goods_count) values ('Orange', 41.50, 900);
insert into goods (description, cost, goods_count) values ('Apple', 13.90, 2100);
insert into goods (description, cost, goods_count) values ('Milk', 22.40, 1300);
insert into goods (description, cost, goods_count) values ('Meat', 60.00, 2050);
insert into goods (description, cost, goods_count) values ('Candies', 105.00, 200);

create table purchaser (
id serial primary key,
name varchar (30)
);

insert into purchaser (name) values ('ООО Печенька');
insert into purchaser (name) values ('ООО Будь Здоров');
insert into purchaser (name) values ('ООО Наш клиент');

create table orders (
id serial primary key,
goods_id integer,
purchaser_id integer,
date_of_order date,
amount_of_goods integer,
foreign key (purchaser_id) references purchaser (id),
foreign key (goods_id) references goods (id)
);

insert into orders (goods_id, purchaser_id, date_of_order, amount_of_goods) values (1, 2, '2021-11-22',500);
insert into orders (goods_id, purchaser_id, date_of_order, amount_of_goods) values (2, 2, '2021-11-22',500);
insert into orders (goods_id, purchaser_id, date_of_order, amount_of_goods) values (3, 2, '2021-11-22',500);

insert into orders (goods_id, purchaser_id, date_of_order, amount_of_goods) values (4, 1, '2021-11-22',300);
insert into orders (goods_id, purchaser_id, date_of_order, amount_of_goods) values (4, 1, '2021-11-29',300);

insert into orders (goods_id, purchaser_id, date_of_order, amount_of_goods) values (1, 3, '2021-11-23',250);
insert into orders (goods_id, purchaser_id, date_of_order, amount_of_goods) values (2, 3, '2021-11-23',250);
insert into orders (goods_id, purchaser_id, date_of_order, amount_of_goods) values (3, 3, '2021-11-24',250);
insert into orders (goods_id, purchaser_id, date_of_order, amount_of_goods) values (4, 3, '2021-11-24',200);
insert into orders (goods_id, purchaser_id, date_of_order, amount_of_goods) values (5, 3, '2021-11-24',400);

/*
select * from purchaser;
select * from goods;
select * from orders;

select description from goods;

select name
from purchaser
where id in (select purchaser_id from orders where orders.amount_of_goods > 300);

select * from orders a full join purchaser p on a.purchaser_id = p.id;

select * from orders a right join goods g on g.id = a.goods_id;

select purchaser, amount_of_goods, description
from purchaser, orders, goods
where purchaser_id = purchaser.id AND orders.id = goods.id ;
*/