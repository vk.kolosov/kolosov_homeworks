package Homework10;

public class Ellipse extends Figure {

    protected double radius1;
    private double radius2;

    public Ellipse(int x, int y, double radius1, double radius2) {
        super(x, y);
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    public double getPerimeter(double radius1, double radius2) {

        double perimeter = 4 * ((Math.PI * radius1 * radius2 + Math.pow(radius1 - radius2, 2)) / (radius1 + radius2));
        return perimeter;
    }
}
