package Homework10;

public class Circle extends Ellipse implements MoveFigure {

    public Circle(int x, int y, double radius) {
        super(x, y, radius, radius);
    }

    public double getPerimeter(double radius) {

        double perimeter = 2 * Math.PI * radius;
        return perimeter;
    }

    @Override
    public void move(int x, int y) {
        this.x = x;
        this.y = y;
        System.out.println("Circle moved to: x = " + this.x + ", y = " + this.y);
    }
}
