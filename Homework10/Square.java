package Homework10;

public class Square extends Rectangle implements MoveFigure{

    public Square(int x, int y, double border) {
        super(x, y, border, border);
    }

    public double getPerimeter(double border) {

        double perimeter = 4 * border;
        return perimeter;
    }

    @Override
    public void move(int x, int y) {
        this.x = x;
        this.y = y;
        System.out.println("Square moved to: x = " + this.x + ", y = " + this.y);
    }
}
