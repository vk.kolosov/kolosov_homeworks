package Homework10;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Random;

public class Main {
    public static void main(String[] args) {

        MoveFigure[] moveFigures = new MoveFigure[10];

        Random rand = new Random();

        for (int i = 0; i < moveFigures.length; i++) {
            if (rand.nextInt(100) > 50) {
                moveFigures[i] = new Circle(1, 1, 2.0);
            } else {
                moveFigures[i] = new Square(1, 2, 2.0);
            }
        }

        for (int i = 0; i < moveFigures.length; i++) {
            moveFigures[i].move(10, 10);
        }

    }
}
