package homework16.linkedlist;

public class Main {

    public static void main(String[] args) {

        LinkedList<String> list = new LinkedList<>();

        list.add("This");
        list.add("is");
        list.add("Java");
        list.add("!");

        System.out.println(list.get(2));

    }
}
