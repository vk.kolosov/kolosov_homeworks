package homework16.arraylist;

public class ArrayList<T> {

    private static final int DEFAULT_SIZE = 10;
    private int size;

    private T[] elements;

    public ArrayList() {
        this.elements = (T[]) new Object[DEFAULT_SIZE];
        this.size = 0;
    }

    private void resize() {
        T[] oldElements = this.elements;
        this.elements = (T[]) new Object[oldElements.length + oldElements.length / 2];
        for (int i = 0; i < oldElements.length; i++) {
            elements[i] = oldElements[i];
        }
    }

    private boolean isFullArray() {
        return size == elements.length;
    }

    public void add(T element) {
        if (isFullArray())
            resize();
        this.elements[size] = element;
        size++;
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index <= size;
    }

    public void clear() {
        this.size = 0;
    }

    public void removeAt(int index) {
        if (isCorrectIndex(index)) {
            this.elements[index] = null;
            for (int i = index; i < size; i++) {
                elements[i] = elements[i + 1];
            }
            size--;
            System.out.println("Element with index " + index + " removed");
        } else {
            System.out.println("Element with index " + index + " not found");
        }
    }

    public void printArray() {
        System.out.print("[");
        for (int i = 0; i < size - 1; i++) {
            System.out.print(elements[i].toString() + ", " + " ");
        }
        System.out.println(elements[size - 1] + "]");
    }

    public void printSize() {
        System.out.println("Current size is " + this.size);
    }

    public void printLength() {
        System.out.println("Current length is " + elements.length);
    }
}
