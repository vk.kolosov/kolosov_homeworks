package homework16.arraylist;

public class Main {

    public static void main(String[] args) {

    ArrayList<Integer> numbers = new ArrayList<>();

    System.out.println("--------------------------------------------");
    System.out.println("Заполнение массива");
    System.out.println("--------------------------------------------");
    numbers.add(0);
    numbers.add(1);
    numbers.add(2);
    numbers.add(3);
    numbers.add(4);
    numbers.add(5);
    numbers.add(6);
    numbers.add(7);
    numbers.add(8);
    numbers.add(9);
    numbers.printArray();
    numbers.printLength();
    numbers.add(10);
    numbers.add(11);
    numbers.add(12);
    numbers.add(13);
    numbers.add(14);
    numbers.add(15);
    numbers.add(16);
    numbers.printArray();
    numbers.printLength();

    System.out.println("--------------------------------------------");
    System.out.println("Удаление элемента");
    System.out.println("--------------------------------------------");
    numbers.removeAt(113);
    numbers.removeAt(13);
    numbers.printArray();
    numbers.printSize();

    System.out.println("--------------------------------------------");
    System.out.println("Добавление еще одного элемента");
    System.out.println("--------------------------------------------");
    numbers.add(17);
    numbers.printArray();
    numbers.printSize();

    }
}
