package homework21;

public class SumThread extends Thread {

    private int from;
    private int to;

    private int sum = 0;

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }

    public SumThread(int from, int to) {
        this.from = from;
        this.to = to;
    }

    public int getSum() {
        return sum;
    }

    @Override
    public void run() {
        for (int i = from; i < to + 1 ; i++) {
            sum += Main.array[i];
        }
        System.out.println("doing from " + from + " to " + to);
    }
}

