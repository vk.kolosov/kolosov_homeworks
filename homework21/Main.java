package homework21;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static int array[];

    public static void main(String[] args) {

        Random rand = new Random();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Type array length: ");
        int numbersCount = scanner.nextInt();

        System.out.println("Type number of threads: ");
        int threadsCount = scanner.nextInt();

        array = new int[numbersCount];

        for (int i = 0; i < numbersCount; i++) {
            array[i] = rand.nextInt(100);
        }

        int sum = 0;
        int threadssum = 0;

        //если пользователь ввел число потоков, превышающее число чисел
        if (threadsCount > numbersCount)
            threadsCount = numbersCount;

        //тут потоки
        SumThread[] list = new SumThread[threadsCount];

        //делим массив чисел на части для каждого потока
        int k = numbersCount / threadsCount; //равномерный интервал
        int lastfrom = k * (threadsCount - 1); //точка отсчета последнего потока

        double nanoTime = System.nanoTime();

        //определение интервалов исключая последний поток
        for (int i = 0; i < threadsCount - 1; i++) {

            int from = k * i;
            int to = from + k - 1;
            list[i] = new SumThread(from, to);
            list[i].start();
        }

        //задание интервала последнего потока с учетом остатка
        list[threadsCount - 1] = new SumThread(lastfrom, numbersCount - 1);
        list[threadsCount - 1].start();

        //сделал sleep, так как sout: threadsum выводит быстрее, чем оно считается по факту
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {throw new IllegalArgumentException(e);}

        /*for (int i = 0; i < threadsCount; i++) {
            System.out.println("k = " + list[i].getFrom() + " " + list[i].getTo());
        }*/

        //суммирование потоками
        for (int j = 0; j < threadsCount; j++) {
            threadssum += list[j].getSum();
        }

        //суммирование обычным способом
        for (int j = 0; j < numbersCount; j++) {
            sum += array[j];
        }

        double nanoTime2 = System.nanoTime();

        System.out.println("Sum from \"main \" is: " + sum);
        System.out.println("Sum from \"threads \" is: " + threadssum);
        System.out.println((nanoTime2 - nanoTime) / 1000000000 + "s");
    }
}
