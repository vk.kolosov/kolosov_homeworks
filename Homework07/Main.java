package Homework07;

import java.util.Arrays;
import java.util.Scanner;

/**
 * добавил расширяющийся массив, немного творческой работы с моей стороны
 */

public class Main {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        //ввод первого значения (вдруг это -1)
        int number = in.nextInt();

        //инициализируем счетчик
        int arrcount = 1;

        //наш массив, заполняем
        int[] arr = new int[arrcount];
        arr[arrcount - 1] = number;

        //вывод первого значения
        System.out.println("Ваш массив: " + Arrays.toString(arr));

        //создание динамического массива
        while (number != -1) {

            number = in.nextInt();

            ++arrcount;

            //создание промежуточного массива
            int[] arr2 = new int[arrcount];

            //копирование старого массива в новый
            for (int i = 0; i < arr.length; i++) {
                arr2[i] = arr[i];
            }
            //добавление позиции в новый массив
            arr2[arrcount - 1] = number;

            //изменяем размер старого массива
            arr = new int[arrcount];

            //заполняем старый массив из промежуточного массива
            for (int i = 0; i < arr2.length; i++) {
                arr[i] = arr2[i];
            }

            System.out.println("Ваш массив: " + Arrays.toString(arr));

        } //конец while, массив заполнен

        //int[] arr = {2, 2, 4, 5, 6, 0, 0, 0, 6, 6, 4, 5}; //все выше можно закомментить и проверить так
        int mincount = 2147483647;
        int min = -1;

        //Решение по значению
        //создаем массив, по которому будет производиться поиск (исключаем -1 из поиска)
        int[] srcharr = new int[200];
        for (int i = -100; i < -1; i++)
            srcharr[i + 100] = i;
        for (int i = 0; i < 101; i++)
            srcharr[99 + i] = i;
        //System.out.println(Arrays.toString(srcharr)); //проверка заполненности массива

        //поиск дубликатов
        for (int i = 0; i < 200; i++) {
            int count = 0; //обнуляем счетчик при каждом поиске
            for (int k = 0; k < arr.length; k++) { //пробегаемся значением поискового массива по текущему
                if (srcharr[i] == arr[k]) {
                    count++;
                }
            }
            if (count < mincount && count > 0) {
                mincount = count;
                min = srcharr[i];
            }
        }

        System.out.println("Минимальное кол-во потороений у числа: " + min + ". Кол-во повторений: " + mincount);
    }
}
