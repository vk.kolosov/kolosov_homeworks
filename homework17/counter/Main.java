package homework17.counter;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        String text = in.nextLine();

        Map<String, Integer> words = new HashMap<>();

        words = Counter.countWords(text);
        Counter.printCounts(words);

    }
}
