package homework17.counter;

import java.util.*;

public class Counter {

    public static Map<String, Integer> countWords(String text) {

        String[] textarray = text.toLowerCase().split(" ");
        Map<String, Integer> result = new HashMap<>();

        for (int i = 0; i < textarray.length; i++) {
            int count = 0;
            for (int j = 0; j < textarray.length; j++) {
                if (textarray[i].equals(textarray[j]))
                    count++;
            }
            result.put(textarray[i], count);
        }

        return result;
    }

    public static void printCounts(Map<String, Integer> map) {

        for (Map.Entry iterated : map.entrySet()) {
            System.out.println(iterated.getKey() + " - " + iterated.getValue());
        }

    }
}
