package homework20;

import java.util.List;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {

        CarReader carReader = new CarReader("src\\homework20\\cars.txt");
        List<Car> carlist = carReader.loadAll();

        for (Car car : carlist) {
            System.out.println("LoadAll: " + car.getCarnumber() + " " + car.getModel() + " " + car.getColor() + " " + car.getKm() + " " + car.getCost());
        }

        Stream<Car> stream = carlist.stream();

        stream.filter(car -> car.getColor().equals("Black") && car.getKm() == 0)
                .forEach(car -> System.out.println("Filter by black color and km = 0: " + car.getCarnumber()));

    }
}
