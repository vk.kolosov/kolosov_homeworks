package homework20;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CarReader {

    private String fileName;

    public CarReader(String fileName) {
        this.fileName = fileName;
    }

    public List<Car> loadAll() {

        List<Car> cars = new ArrayList<>();
        FileReader reader = null;
        BufferedReader bufferedReader = null;

        try {
            reader = new FileReader(fileName);
            bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();

            while (line != null) {

                String[] carlist = line.split("\\|");

                String carnumber = carlist[0];
                String model = carlist[1];
                String color = carlist[2];
                int km = Integer.parseInt(carlist[3]);
                int cost = Integer.parseInt(carlist[4]);

                Car newCar = new Car(carnumber, model, color, km, cost);
                cars.add(newCar);

                line = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException();

        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ignore) {
                }
            }
        }
        return cars;
    }
}
