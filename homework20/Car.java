package homework20;

import java.util.Objects;

public class Car {

    private String carnumber;
    private String model;
    private String color;
    private int km;
    private int cost;

    public Car(String carnumber, String model, String color, int km, int cost) {
        this.carnumber = carnumber;
        this.model = model;
        this.color = color;
        this.km = km;
        this.cost = cost;
    }

    public String getCarnumber() {
        return carnumber;
    }

    public void setCarnumber(String carnumber) {
        this.carnumber = carnumber;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return km == car.km && cost == car.cost && Objects.equals(carnumber, car.carnumber) && Objects.equals(model, car.model) && Objects.equals(color, car.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(carnumber, model, color, km, cost);
    }
}
