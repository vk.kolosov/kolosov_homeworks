public class NumbersUtil {
    /*
    нод(18, 12) -> 6
    нод(9, 12) -> 3
    нод(64, 48) -> 16

    Предусмотреть, когда на вход "некрсивые числа", отрицательные числа -> исключения
     */
    public int gcd(int a, int b) {

        if (a == 0 || a == 1 || a == -1 || b == 0 || b == 1 || b == -1 || a == b || (a < 0 && b > 0) || (a > 0 && b < 0)) {
            throw new IllegalArgumentException();
        }

        //Для отрицательных чисел
        a = Math.abs(a);
        b = Math.abs(b);

        while (a != 0 && b != 0) {
            if (a > b)
                a = a % b;
            else
                b = b % a;
        }
        return a + b;
    }
}
