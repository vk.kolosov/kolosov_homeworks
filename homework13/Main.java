package Homework13;


import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        int[] nums = {22, 45, 332, 44, 55, 9898, 988};

        ByCondition condition = new ByCondition() {
            @Override
            public boolean isOk(int number) {

                boolean cond = false;
                int sum = 0;
                int mediate = number;

                while (mediate != 0) {

                    sum = sum + mediate % 10;
                    mediate = mediate / 10;

                }

                if (number % 2 == 0 && sum % 2 == 0)
                    cond = true;

                return cond;
            }
        };

        //упростил выражение с лимитом до 9999, чтобы не громоздить циклом while
        //ByCondition conditiontest = x -> x % 2 == 0 && (x % 10 + (x % 100) / 10 + ((x % 1000) / 100) + (x % 10000 / 1000)) % 2 == 0;

        int[] a = Sequence.filter(nums, condition);
        int[] b = Sequence.filter(nums, number -> number % 2 == 0 && (number % 10 + (number % 100) / 10 + ((number % 1000) / 100) + (number % 10000 / 1000)) % 2 == 0);

        System.out.println("Starting array: " + Arrays.toString(nums));
        System.out.println("Usual way: " + Arrays.toString(a));
        System.out.println("With lambda: " + Arrays.toString(b));
    }
}