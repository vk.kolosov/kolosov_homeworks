package Homework13;

public class Sequence {

    public static int[] filter(int[] array, ByCondition condition) {

        int[] result = new int[array.length];
        int count = 0;

        //заполнение массива четными числами
        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {

                result[count] = array[i];
                count++;
            }
        }

        //убирание null
        int[] finalresult = new int[count];

        for (int i = 0; i < count ; i++) {
            finalresult[i] = result [i];
        }

        return finalresult;
    }
}
