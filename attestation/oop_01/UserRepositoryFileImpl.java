package attestation.oop_01;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class UserRepositoryFileImpl implements UserRepository {

    private static String fileName;
    private static List<User> users;

    public UserRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {

        users = new ArrayList<>();
        Reader reader = null;
        BufferedReader bufferedReader = null;

        try {
            reader = new FileReader(fileName);
            bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();

            while (line != null) {

                String[] parts = line.split("\\|");

                int id = Integer.parseInt(parts[0]);
                String name = parts[1];
                int age = Integer.parseInt(parts[2]);
                boolean isWorker = Boolean.parseBoolean(parts[3]);

                User newUser = new User(name, age, isWorker);
                newUser.setId(id);
                users.add(newUser);
                line = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);

        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ignore) {
                }
            }
        }
        return users;
    }

    @Override
    public void printAll() {
        //считываем файл
        users = findAll();
        if (users.size() < 1)
            System.out.println("Список пуст");
        else {
            for (User eachuser : users)
                System.out.println("Текущие данные: " + eachuser.getId() + " " + eachuser.getName() + " " + eachuser.getAge() + " " + eachuser.isWorker());
        }
    }

    private void saveList() {

        Writer writer = null;
        BufferedWriter bufferedWriter = null;

        try {
            writer = new FileWriter(fileName);
            bufferedWriter = new BufferedWriter(writer);

            for (User eachuser : users) {
                bufferedWriter.write((users.indexOf(eachuser) + 1) + "|" + eachuser.getName() + "|" + eachuser.getAge() + "|" + eachuser.isWorker());
                bufferedWriter.newLine();
                bufferedWriter.flush();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);

        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ignore) {
                }
            }
        }
    }

    @Override
    public void save(User user) {
        //считываем файл
        users = findAll();
        //добавлем в скачанный список новый элемент
        users.add(user);
        int ind = users.lastIndexOf(user);
        //установка индекса
        user.setId(ind + 1);
        System.out.println("Добавлен: " + users.get(ind).getId() + " " + users.get(ind).getName() + " " + users.get(ind).getAge() + " " + users.get(ind).isWorker());
        //запись массива в файл
        saveList();
    }

    @Override
    public List<User> findByAge(int userage) {

        List<User> findusers = new ArrayList<>();
        users = findAll();

        for (User eachuser : users) {
            if (eachuser.getAge() == userage)
                findusers.add(eachuser);
        }

        return findusers;
    }

    @Override
    public List<User> findByIsWorkerIsTrue() {

        List<User> findusers = new ArrayList<>();
        users = findAll();

        for (User eachuser : users) {
            if (eachuser.isWorker() == true)
                findusers.add(eachuser);
        }

        return findusers;
    }

    @Override
    public User findById(int id) {

        User targetUser = null;
        users = findAll();

        for (User eachuser : users) {
            if (eachuser.getId() == id)
                targetUser = eachuser;
        }

        System.out.println("Юзер найден: " + targetUser.getId() + " " + targetUser.getName() + " " + targetUser.getAge() + " " + targetUser.isWorker());
        return targetUser;
    }

    @Override
    public void update(User user) {
        //считываем файл
        users = findAll();
        //удаляем по индексу
        int ind = user.getId() - 1;
        users.remove(ind);
        //добавляем по индексу
        users.add(ind, user);
        System.out.println("Обновлен: " + users.get(ind).getId() + " " + users.get(ind).getName() + " " + users.get(ind).getAge() + " " + users.get(ind).isWorker());
        //запись массива в файл
        saveList();
    }

    @Override
    public void removeUser(int id) {

        users = findAll();
        int targetID = id - 1;
        System.out.println("Юзер удален: " + users.get(targetID).getId() + " " + users.get(targetID).getName() + " " + users.get(targetID).getAge() + " " + users.get(targetID).isWorker());
        users.remove(targetID);
        saveList();
    }
}





