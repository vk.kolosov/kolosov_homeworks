package attestation.oop_01;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        //инициализируем
        UserRepository userRepository = new UserRepositoryFileImpl("src\\attestation\\oop_01\\users.txt");
        //выводим данные из файла
        userRepository.printAll();

        User user1 = new User("Bob", 21, true);
        userRepository.save(user1);

        User user2 = new User("Fred", 24, true);
        userRepository.save(user2);

        User user3 = new User("Tommy", 19, false);
        userRepository.save(user3);

        User targetUser = userRepository.findById(2);
        targetUser.setName("new " + targetUser.getName());
        targetUser.setAge(5 + targetUser.getAge());
        userRepository.update(targetUser);

        List<User> usersIsWorker = userRepository.findByIsWorkerIsTrue();

        for (User user: usersIsWorker) {
            System.out.println("IsWorker - true: " + user.getAge() + " " + user.getName() + " " + user.isWorker());
        }

        userRepository.removeUser(3);
    }
}
