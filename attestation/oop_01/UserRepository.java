package attestation.oop_01;
import java.util.List;

public interface UserRepository {

    List<User> findAll();
    void save(User user);
    List<User> findByAge(int userage);
    List<User> findByIsWorkerIsTrue();

    //attestation
    User findById(int id);
    void printAll();
    void update(User user); //change
    void removeUser(int id);

}
