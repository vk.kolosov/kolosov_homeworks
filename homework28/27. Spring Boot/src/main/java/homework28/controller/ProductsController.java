package homework28.controller;

import homework28.models.Product;
import homework28.repositories.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProductsController {
    // DI
    @Autowired
    private ProductsRepository productsRepository;

    @PostMapping("/products")
    public String addProduct(@RequestParam("description") String description,
                             @RequestParam("cost") double cost,
                             @RequestParam("goods_count") Integer goods_count,
                             Model model)

            //model - подставляет что-то

    {

        Product product = Product.builder()
                .decription(description)
                .cost(cost)
                .goods_count(goods_count)
                .build();

        System.out.println(product.getDecription() + " " + product.getCost());
        productsRepository.save(product);

        return "redirect:/product_add.html";
    }

    @PostMapping("/update_cost")
    public String updateCost(@RequestParam("description") String description,
                             @RequestParam ("cost") double cost)

    {
        System.out.println("New cost for " + description + " is: " + cost);
        productsRepository.update(description, cost);

        return "redirect:/product_add.html";
    }
}
