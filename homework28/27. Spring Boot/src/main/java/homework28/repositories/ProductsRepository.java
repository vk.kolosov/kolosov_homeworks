package homework28.repositories;

import homework28.models.Product;

import java.util.List;

public interface ProductsRepository {

    List<Product> findAll();
    List<Product> findAllByPrice(double price);
    void save(Product product);
    void update(String description, double cost);

}
