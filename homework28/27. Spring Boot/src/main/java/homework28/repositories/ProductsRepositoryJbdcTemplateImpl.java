package homework28.repositories;

import homework28.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;

@Component
public class ProductsRepositoryJbdcTemplateImpl implements ProductsRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from goods order by id";

    //language=SQL
    private static final String SQL_SELECT_BY_PRICE = "select * from goods where cost = ";

    //language=SQL
    private static final String SQL_INSERT = "insert into goods(description, cost, goods_count) values(?, ?, ?)";

    //language=SQL
    private static String SQL_UPDATE = "update goods set cost = ? where description = ?";

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public ProductsRepositoryJbdcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    RowMapper<Product> productRowMapper = (row, rowNum) -> {
        int id = row.getInt("id");
        String description = row.getString("description");
        double cost = row.getDouble("cost");
        int goods_count = row.getInt("goods_count");
        return new Product(id, description, cost, goods_count);
    };

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        return jdbcTemplate.query(SQL_SELECT_BY_PRICE + price, productRowMapper);
    }

    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_INSERT, product.getDecription(), product.getCost(), product.getGoods_count());
    }

    @Override
    public void update(String description, double cost) {
        jdbcTemplate.update(SQL_UPDATE, cost, description);
    }
}
