create table goods (
id serial primary key,
description varchar (20),
cost double precision,
goods_count integer
);

insert into goods (description, cost, goods_count) values ('Banana', 30.50 , 1400);
insert into goods (description, cost, goods_count) values ('Orange', 41.50, 900);
insert into goods (description, cost, goods_count) values ('Apple', 13.90, 2100);
insert into goods (description, cost, goods_count) values ('Milk', 22.40, 1300);
insert into goods (description, cost, goods_count) values ('Meat', 60.00, 2050);
insert into goods (description, cost, goods_count) values ('Candies', 105.00, 200);

