package Homework08;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {


        Scanner in = new Scanner(System.in);

        System.out.println("Желаемый размер массива: ");

        //Создаем массив на 'count' человек
        int count = in.nextInt(); //кол-во людей на ввод
        Human[] humans = new Human[count];

        System.out.println("Введите имя " + count + " сотрудников и их вес (Пример 'Джон Галт 55,2'): ");

        for (int i = 0; i < count; i++) {

            String outname = in.next(); //конкретно под String не нашел
            outname = outname + " " + in.next(); //для фамилии
            double outnum = in.nextDouble();

            humans[i] = new Human();
            humans[i].setName(outname);
            humans[i].setWeight(outnum);

        }

        //Создание промежуточного значения для сортировки
        Human tmp = new Human();

        //Сортировка выборкой по весу
        for (int i = 0; i < count; i++) {
            double min = humans[i].getWeight();
            int minIndex = i;
            for (int j = i + 1; j < count; j++) {
                if (min > humans[j].getWeight()) {
                    min = humans[j].getWeight();
                    minIndex = j;
                }
            }
            tmp = humans[i];
            humans[i] = humans[minIndex];
            humans[minIndex] = tmp;
        }

        //Вывод отсортированного массива
        System.out.println("Вывод: ");
        for (int i = 0; i < count; i++) {
            System.out.println(humans[i].getName() + " " + humans[i].getWeight());
        }
    }
}

