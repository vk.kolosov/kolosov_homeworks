package Homework08;

public class Human {

    private String name;
    private double weight;

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(double weight) {
        if (weight < 0) {
            this.weight = 0;
        } else {
            this.weight = weight;
        }
    }

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }
}