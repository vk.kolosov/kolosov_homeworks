package Homework06;

import java.util.Scanner;

public class Indexofintinarray {

    public static void main(String[] args) {

        //задать размер массива
        Scanner in  = new Scanner(System.in);

        System.out.println("Введите размер массива: ");
        int lnght = in.nextInt();
        int[] a = new int[lnght];

        //ввести массив
        System.out.println("Введите массив: ");
        for (int i = 0; i < a.length; i++) {
            int k = in.nextInt();
            a[i] = k;
        }

        //вывод массива
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }

        //переход на новую строчку
        System.out.println();

        //выбор числа для поиска его индекса
        System.out.println("Введите число, индекс которого желаете узнать: ");
        int b = in.nextInt();

        //int[] a = {3, 2, 1, 0, 6, 0, 8}; все выше можно закомментить и проверить так
        //int b = 1;

        //проверка функции
        System.out.println("Поиск индекса " + b + "...");
        int index = getIndexFromArray(a, b);
        System.out.println("Индекс " + b + ": " + index);

        //проверка процедуры
        replaceNullToRight(a);

    }

    public static int getIndexFromArray (int[] a, int b)
    {
        int index = -1;

        for (int i = 0; i < a.length; i++)
        {
            if (b == a[i])
            {
                index = i;
            }
        }
        return index;
    }

    public static void replaceNullToRight (int [] a)
    {
        int nullcount = 0;

        //определим количество нулей в массиве
        for (int i = 0; i < a.length; i++) {
            if (a[i] == 0)
            {
            nullcount++;
            }
        }

        //создаем два второстепенных массива (с целыми числами и без)
        int[] a1 = new int[a.length-nullcount];
        int[] anull = new int[nullcount];

        //счетчики заполнения массива
        int a1num = 0;
        int anullnum = 0;

        //пробегаемся по списку и заполняем массивы
        for (int i = 0; i < a.length; i++) {
              if (a[i] != 0){
                  a1[a1num] = a[i];
                  a1num++;
              } else {
                  anull[anullnum] = a[i];
                  anullnum++;
              }
        }

        //объединяем два массива
        for (int i = 0; i < a1.length; i++) {
            a[i] = a1[i];
        }
        for (int i = 0; i < anull.length; i++) {
            a[i + a1.length] = anull[i];
        }

        //вывод массива на экран
        System.out.println("Сортировка массива...");
        System.out.print("Результат сортировки: ");
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
    }
}
