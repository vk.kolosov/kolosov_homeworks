package Homework05;

import java.util.Scanner;

public class Minimumfrominput {
    public static void main(String[] args) {

        Scanner in  = new Scanner(System.in);

        int number = in.nextInt();
        int minimum = 10;

        while (number != -1){

            while (number !=0) {

                if (minimum > number % 10)
                {
                    minimum = number % 10;
                }

                number = number / 10;
            }

            System.out.println("Current minimum is " + minimum);
            number = in.nextInt();
        }

        System.out.println("Program finished");
        System.out.println("Final minimum is " + minimum);
    }
}
