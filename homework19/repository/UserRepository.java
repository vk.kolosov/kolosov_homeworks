package homework19.repository;

import java.util.List;

public interface UserRepository {

    List<User> findAll();
    void save(User user);
    List<User> findByAge(int userage);
    List<User> findByIsWorkerIsTrue();

}
