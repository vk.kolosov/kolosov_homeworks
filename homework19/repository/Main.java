package homework19.repository;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        UserRepository usersRepository = new UserRepositoryFileImpl("src\\homework19\\repository\\users.txt");

        List<User> users = usersRepository.findAll();

        for (User user : users) {
            System.out.println("FindAll: " + user.getAge() + " " + user.getName() + " " + user.isWorker());
        }

        List<User> usersAge = usersRepository.findByAge(28);

        for (User user: usersAge) {
            System.out.println("FindByAge: " + user.getAge() + " " + user.getName() + " " + user.isWorker());
        }

        List<User> usersIsWorker = usersRepository.findByIsWorkerIsTrue();

        for (User user: usersIsWorker) {
            System.out.println("IsWorker - true: " + user.getAge() + " " + user.getName() + " " + user.isWorker());
        }

        User user = new User("Игорь", 33, true);
        usersRepository.save(user);

    }
}
