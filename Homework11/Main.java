package Homework11;

public class Main {

    public static void main(String[] args) {

        Logger logger1 = Logger.getInstance(); //ссылка на единственный
        Logger logger2 = Logger.getInstance(); //эксземляр instance, который
                                               //уже создан внутри класса Logger

        logger1.log("Hello!");
        logger2.log("Hello again!");

        logger1.setValue(2);
        logger2.setValue(4);

        logger1.showValue();
        logger2.showValue();
    }
}
