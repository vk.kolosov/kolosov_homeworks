package Homework11; //паттерн Singleton

public class Logger {

    private static final Logger instance; //специальное статическое поле типа Logger

    static {
        instance = new Logger(); //инициализация класса в статическом поле(единственная)
    }

    private Logger() { //приватный конструктор (обязателен ли он?)
    }

    int a;

    public static Logger getInstance() {
        return instance;
    }

    public void log(String message) {
        System.out.println(message);
    }

    public void setValue(int a) {
        this.a = a;
    }

    public void showValue() {
        System.out.println("a = " + a);
    }
}
