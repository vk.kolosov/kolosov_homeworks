package homework28.security.details;

import homework28.models.Purchaser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

public class PurchaserDetailsImpl implements UserDetails { //Юзер детайлс работает непосредственно со Спринг Секьюрити

    private final Purchaser purchaser;

    public PurchaserDetailsImpl(Purchaser purchaser) {
        this.purchaser = purchaser;
    }

    @Override //когда у пользователя спросят роль, возьмем то, что записано в базе
              //и отправим в СпрингСекьюрити, т.е. GrantedAuthority
    public Collection<? extends GrantedAuthority> getAuthorities() {
        String role = purchaser.getRole().toString();
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role);
        return Collections.singleton(authority);
    }

    @Override
    public String getPassword() {
        return purchaser.getHashPassword();
    }

    @Override
    public String getUsername() {
        return purchaser.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() { //Не просрочен ли аккаунт
        return true;
    }

    @Override
    public boolean isAccountNonLocked() { //Не заблокирован ли аккаунт
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() { //Не просрочен ли пароль
        return true;
    }

    @Override
    public boolean isEnabled() { //Аккаунт активный или нет
        return true;
    }
}
