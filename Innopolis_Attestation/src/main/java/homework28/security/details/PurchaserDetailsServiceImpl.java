package homework28.security.details;

import homework28.models.Purchaser;
import homework28.repositories.PurchaserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class PurchaserDetailsServiceImpl implements UserDetailsService {

    private final PurchaserRepository purchaserRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Purchaser purchaser = purchaserRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("User not found"));
        PurchaserDetailsImpl purchaserDetails = new PurchaserDetailsImpl(purchaser);
        return purchaserDetails;
    }
}
