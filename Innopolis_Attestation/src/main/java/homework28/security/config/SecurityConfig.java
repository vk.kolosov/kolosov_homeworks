package homework28.security.config;

import homework28.security.details.PurchaserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter { //Связь spring security с нашими классами

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDetailsService purchaserDetailsServiceImpl;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(purchaserDetailsServiceImpl).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable();

        http.authorizeRequests()
                .antMatchers("/to_order").authenticated()
                .antMatchers("/product").hasAuthority("ADMIN")
                .antMatchers("/signUp").permitAll()
                .and()
                .formLogin()
                //.loginProcessingUrl("/signIn")
                .loginPage("/signIn")
                .defaultSuccessUrl("/to_order")
                //.failureForwardUrl("/signIn?error")
                .usernameParameter("email")
                .passwordParameter("hashPassword")
                .permitAll();
    }
}
