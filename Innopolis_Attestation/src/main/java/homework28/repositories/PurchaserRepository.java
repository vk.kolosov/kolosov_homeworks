package homework28.repositories;

import homework28.models.Orders;
import homework28.models.Product;
import homework28.models.Purchaser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PurchaserRepository extends JpaRepository<Purchaser, Integer> {

    Purchaser findPurchaserByName(String name);
    Purchaser findPurchaserByEmail (String email);

    Purchaser getByName(String name);
    Purchaser getByEmail(String name);

    Optional<Purchaser> findByEmail(String email);

}
