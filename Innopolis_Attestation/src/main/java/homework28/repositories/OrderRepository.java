package homework28.repositories;

import homework28.models.Orders;
import homework28.models.Product;
import homework28.models.Purchaser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Orders, Integer> {

    List<Orders> findAll(); //работает

    List<Orders> findAllByProductId(Product product);

    List<Orders> findAllByPurchaser(Purchaser purchaser);

}
