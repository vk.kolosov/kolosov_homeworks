package homework28.controller;

import homework28.forms.ProductForm;
import homework28.models.Orders;
import homework28.models.Product;
import homework28.models.Purchaser;
import homework28.repositories.ProductRepository;
import homework28.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;

@Controller
public class ProductController {
    // DI
    @Autowired
    private ProductRepository productsRepository;

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/home")
    public String getYourDestiny(){
        return "home";
    }

    //Админка - работает
    @GetMapping("/product") //прямой адрес в браузере
    public String getStockPage(Model model) {
        List<Product> productList = productService.findAll();
        model.addAttribute("product_attribute", productList);
        return "products_stock"; //возвращает шаблон
    }

    //Добавляет товар но форме товара: description, cost, goods_count
    //В .addProduct стоит builder на эту форму
    @PostMapping("/products")
    public String addProduct(ProductForm form) {
        productService.addProduct(form);
        return "redirect:/product"; //уходит на GetMapping /products и возвращает /products_stock
    }

    //Апдейтит цену по одному введенному параметру + id взят из ${product.id}
    @PostMapping("/update_cost")
    public String updateCost(@RequestParam("id") Integer id,
                             @RequestParam("cost") double cost) {
        System.out.println("New cost for " + id + " is: " + cost);
        productService.updateCost(id, cost);

        return "redirect:/product";
    }

    //Удаляет товар из базы по введенному id
    @PostMapping("/remove")
    public String removeProdut(@RequestParam("id") Integer id) {
        productService.delete(id);
        return "redirect:/product";
    }

    //Апдейтит цену по одному введенному параметру + id взят из ${product.id}
    @PostMapping("/addcount")
    public String addCount(@RequestParam("id") Integer id,
                           @RequestParam("goods_count") Integer goods_count) {
        System.out.println(id + " " + goods_count);
        productService.addCount(id, goods_count);
        return "redirect:/product";
    }

    //Выводит заказы по продукту
    @GetMapping("products/{product-id}/orders")
    public String getOrdersByProducts(Model model, @PathVariable("product-id") Integer id) //соотношение @PathVariable в http с полем id
    {
        List<Orders> orderList = productService.findAllByGoodId(id);
        model.addAttribute("ordersbyproducts_attribute", orderList);
        Product product = productService.getProduct(id);
        model.addAttribute("product", product);
        return "sample";
    }

    //Поехали //Клиент переходит сюда //Показывает товар на складе
    @GetMapping("/to_order") //прямой адрес в браузере
    public String getOrderPage(Model model) {
        List<Product> productList = productService.findAll();
        model.addAttribute("product_attribute", productList);
        return "to_order"; //возвращает шаблон
    }

    //Выводит товар на складе с возможностью создать заказ на этого пользователя
    @GetMapping("/to_order/market")
    public String showAllForPurchaser(Model model, @AuthenticationPrincipal UserDetails currentUser) {
        List<Product> productList = productService.findAll();
        model.addAttribute("product_attribute", productList);
        List<Orders> orderList = productService.findAllByPurchaserEmail(currentUser.getUsername());
        model.addAttribute("ordersbyproducts_attribute", orderList);
        //Purchaser purchaser = productService.getPurchaser(currentUser.getUsername());
        model.addAttribute("purchaser", productService.getPurchaser(currentUser.getUsername()));
        return "purchaser_show_goods";
    }

    //Добавляет в заказ, удаляет со склада
    @PostMapping("/add_to_order")
    public String addToCurrentOrders(@AuthenticationPrincipal UserDetails currentUser,
                                     @RequestParam("id") Integer productId,
                                     @RequestParam("goods_count") Integer countToOrder) {
        //Purchaser purchaser = productService.getPurchaser(currentUser.getUsername());
        //String name = purchaser.getName();
        productService.addToOrder(productId, productService.getPurchaser(currentUser.getUsername()).getName(), countToOrder);
        return "redirect:/to_order/market";
    }

}
