package homework28.forms;

import lombok.Data;

@Data
public class ProductForm {

    private Integer id;
    private String description;
    private double cost;
    private Integer goods_count;

}
