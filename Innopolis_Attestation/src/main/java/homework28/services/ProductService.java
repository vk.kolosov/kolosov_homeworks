package homework28.services;

import homework28.forms.ProductForm;
import homework28.models.Orders;
import homework28.models.Product;
import homework28.models.Purchaser;

import java.util.List;

public interface ProductService {

    List<Product> findAll();

    Product getProduct(Integer productID);

    void addProduct(ProductForm form);

    void addCount(Integer Id, Integer count);

    void updateCost(Integer Id, double cost);

    void delete(Integer id);

    void save (Product product);

    List<Orders> findAllByGoodId(Integer id);

    List<Orders> findAllByPurchaserEmail(String name);

    Purchaser getPurchaser(String mail);

    void addToOrder(Integer productId, String purchaserName, Integer newCount);
}
