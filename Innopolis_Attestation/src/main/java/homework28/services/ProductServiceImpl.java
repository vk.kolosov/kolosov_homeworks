package homework28.services;

import homework28.forms.ProductForm;
import homework28.models.Orders;
import homework28.models.Product;
import homework28.models.Purchaser;
import homework28.repositories.OrderRepository;
import homework28.repositories.ProductRepository;
import homework28.repositories.PurchaserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@RequiredArgsConstructor
@Component
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final OrderRepository orderRepository;
    private final PurchaserRepository purchaserRepository;

    //@RequiredArgsConstructor - сам поставит нужный конструктор - Lombok
    /*@Autowired
    public ProductServiceImpl(ProductsRepository productsRepository, OrdersRepository ordersRepository) {
        this.productsRepository = productsRepository;
        this.ordersRepository = ordersRepository;
    }*/

    @Override
    public void addProduct(ProductForm form) {

        Product product = Product.builder()
                .description(form.getDescription())
                .cost(form.getCost())
                .goodCount(form.getGoods_count())
                .build();

        productRepository.save(product);

    }

    @Override
    //выгрузка отсортированного по ID массива
    public List<Product> findAll() {
        List<Product> products = productRepository.findAll();

        Collections.sort(products, new Comparator<Product>() { //или Comparator.comparingInt(Product::getId)
            @Override
            public int compare(Product o1, Product o2) {
                return o1.getId() - o2.getId();
            }
        });

        return products;
    }

    @Override
    public void save(Product product) {
        productRepository.save(product);
    }

    @Override
    public void addCount(Integer Id, Integer count) {
        Product product = productRepository.findProductById(Id);
        Integer newcount = count + product.getGoodCount();
        product.setGoodCount(newcount);
        productRepository.save(product);
    }

    @Override
    public void updateCost(Integer Id, double cost) {
        Product product = productRepository.findProductById(Id);
        product.setCost(cost);
        productRepository.save(product);
    }

    @Override
    public void delete(Integer id) {
        productRepository.deleteById(id);
    }

    @Override
    public List<Orders> findAllByGoodId(Integer id) {
        Product product = productRepository.findProductById(id);
        return orderRepository.findAllByProductId(product);

    }

    @Override
    public Product getProduct(Integer productID) {
        return productRepository.getById(productID);
    }

    public List<Orders> findAllByPurchaserEmail(String name) {
        Purchaser purchaser = purchaserRepository.findPurchaserByEmail(name);
        return orderRepository.findAllByPurchaser(purchaser);
    }

    @Override
    public Purchaser getPurchaser(String mail) {
        return purchaserRepository.getByEmail(mail);
    }

    @Override
    public void addToOrder(Integer productId, String purchaserName, Integer count) {
        //убираем со склада
        Product product = productRepository.findProductById(productId);
        Integer newcount = product.getGoodCount() - count;
        product.setGoodCount(newcount);
        productRepository.save(product);

        //определение Purchaser
        Purchaser purchaser = purchaserRepository.getByName(purchaserName);
        //добавляем в заказ
        Date today = new Date();
        java.sql.Date sqltoday = new java.sql.Date(today.getTime());

        Orders order = Orders.builder()
                .productId(product)
                .purchaser(purchaser)
                .dateOfOrder(sqltoday)
                .amountOfGoods(count)
                .build();

        orderRepository.save(order);
    }

}
