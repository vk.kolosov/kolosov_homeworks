package homework28.services;

import homework28.forms.SignUpForm;

public interface SignUpService {

    void signUpUser(SignUpForm form);

}
