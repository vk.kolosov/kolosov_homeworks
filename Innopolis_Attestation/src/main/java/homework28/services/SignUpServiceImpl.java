package homework28.services;

import homework28.forms.SignUpForm;
import homework28.models.Purchaser;
import homework28.repositories.PurchaserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
@RequiredArgsConstructor
@Component
public class SignUpServiceImpl implements SignUpService {

    private final PasswordEncoder passwordEncoder; //Требует конструктор, надо объявить -- @RequiredArgsConstructor
    private final PurchaserRepository purchaserRepository;

    @Override
    public void signUpUser(SignUpForm form) {
        Purchaser purchaser = Purchaser.builder()
                .role(Purchaser.Role.USER)
                .name(form.getName())
                .email(form.getEmail())
                .hashPassword(passwordEncoder.encode(form.getHashPassword()))
                .build();
        purchaserRepository.save(purchaser);
    }

}
