package homework28.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Purchaser {

    public enum Role{
        ADMIN, USER;
    }

    @Enumerated(value = EnumType.STRING)
    private Role role;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    @OneToMany(mappedBy = "id") //поле, по которому связана таблица
    private List<Orders> orderList;

    @Column(unique = true)
    private String email;
    @Column(name = "hash_password")
    private String hashPassword;

}
