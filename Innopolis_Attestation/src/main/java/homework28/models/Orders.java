package homework28.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "orders")
public class Orders {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "goods_id") //ссылка на колонку привязываемой таблицы
    //private Product product;
    private Product productId;

    @ManyToOne
    @JoinColumn(name = "purchaser_id")
    private Purchaser purchaser;

    @Column(name = "date_of_order")
    private Date dateOfOrder;
    //java.sql.Date just represent DATE without time information while java.util.Date represents both Date and Time information. This is the major difference why java.util.Date can not directly map to java.sql.Date.
    @Column(name = "amount_of_goods")
    private Integer amountOfGoods;
}
