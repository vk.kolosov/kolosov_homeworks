package homework28.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "goods")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //есть ID, он генерируется базой данных

    private Integer id;

    private String description;

    private double cost;

    @Column(name = "goods_count")
    private Integer goodCount;

    @OneToMany(mappedBy = "id")
    private List<Orders> productList;
}
