package Homework09;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class Main {
    public static void main(String[] args) {

        Figure fig1 = new Figure(1, 1);
        Ellipse ell1 = new Ellipse(1, 1, 2.0, 3.0);
        Circle cir1 = new Circle(1, 1, 2.0);
        Rectangle rec1 = new Rectangle(1, 1, 2.0, 3.0);
        Square squ1 = new Square(1, 1, 2.0);

        //результаты с Math.PI выводятся очень громоздкими
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setDecimalSeparator('.');
        DecimalFormat decimalFormat = new DecimalFormat("#.#", decimalFormatSymbols);

        System.out.println("Периметр фигуры по умолчанию: " + fig1.getPerimeter(1, 1));
        //если на вход подавать int, то подумает, что это Figure
        System.out.println("Периметр эллипса: " + decimalFormat.format(ell1.getPerimeter(2.0, 3.0)));
        System.out.println("Периметр окружности: " + decimalFormat.format(cir1.getPerimeter(2.0)));

        /* в этом примере не смог найти вариант с "."
        System.out.printf("Периметр окружности: %.1f", cir1.getPerimeter(2.0));
        System.out.println();
        //вывод: 12,6
        */

        System.out.println("Периметр прямоугольника: " + rec1.getPerimeter(2.0, 3.0));
        System.out.println("Периметр квадрата: " + squ1.getPerimeter(2.0));

    }
}
