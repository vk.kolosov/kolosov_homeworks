package Homework09;

public class Rectangle extends Figure {

    protected double width;
    private double height;

    public Rectangle(int x, int y, double width, double height) {
        super(x, y);
        this.width = width;
        this.height = height;
    }

    public double getPerimeter(double width, double height) {

        double perimeter = 2 * (width * height);
        return perimeter;
    }
}
