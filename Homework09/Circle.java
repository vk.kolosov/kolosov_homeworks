package Homework09;

public class Circle extends Ellipse {

    public Circle(int x, int y, double radius) {
        super(x, y, radius, radius);
    }

    public double getPerimeter(double radius) {

        double perimeter = 2 * Math.PI * radius;
        return perimeter;
    }
}
