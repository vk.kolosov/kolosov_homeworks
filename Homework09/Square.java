package Homework09;

public class Square extends Rectangle {

    public Square(int x, int y, double border) {
        super(x, y, border, border);
    }

    public double getPerimeter(double border) {

        double perimeter = 4 * border;
        return perimeter;
    }

}
